# coding: utf-8
from grab import Grab
import re
import tornado.ioloop
import tornado.web
import webbrowser

g = Grab()


def work_with_response_list(response_list):
    text_page = ''
    for word in response_list:
        if len(word) == 6 and re.search("[`'\",.:;0-9_]$", word) == None:
            text_page = text_page+word+u'™'+' '
        else:
            text_page = text_page+word+' '
    return text_page


def get_text_page(url):
    if url == "/":
        g.go('http://habrahabr.ru/')
        posts_in_index_page = ''
        for elem in g.doc.select('//div[@class="content html_format"]'):
            posts_in_index_page = posts_in_index_page + \
                work_with_response_list(elem.text().split(' '))+'<br><br>'
        return posts_in_index_page

    g.go('http://habrahabr.ru'+url)
    response_list = g.doc.select(
        '//div[@class="content html_format"]').text().split(' ')

    return work_with_response_list(response_list)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write(get_text_page(self.request.uri))


class PostHandler(tornado.web.RequestHandler):
    def get(self, *arg, **kwargs):
        self.write(get_text_page(self.request.uri))

application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/(\w+)/([0-9]+)/", PostHandler),
    (r"/(\w+)/([0-9]+)", PostHandler),
    (r"/(\w+)/(\w+)/([0-9]+)/", PostHandler),
    (r"/(\w+)/(\w+)/([0-9]+)", PostHandler),
    (r"/(\w+)/(\w+)/(\w+)/([0-9]+)", PostHandler),
    (r"/(\w+)/(\w+)/(\w+)/([0-9]+)/", PostHandler),
])

if __name__ == "__main__":
    application.listen(8000)
    webbrowser.open("http://127.0.0.1:8000/")
    tornado.ioloop.IOLoop.current().start()

